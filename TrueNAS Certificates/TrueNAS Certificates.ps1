﻿Set-Location $PSScriptRoot

### Initializes Variables
# Path of the File containing stored Variables
$ConfigPath = ".\Ressources\config.txt"

. ..\_shared\Get-Config.ps1

. Get-Config -ConfigPath $ConfigPath -Delimiter "="

### Declare Functions
# Function to Store Variables in a File
. ..\_shared\Set-Config.ps1


### Declare Variables
$ServerSocket = ($ServerProtocol + "://" + $ServerName + ":" + $ServerPort)

$APIKey = $APIKey | ConvertTo-SecureString -AsPlainText -Force
$APICertificate = "/api/v2.0/certificate/"
$APISystemGeneral = "/api/v2.0/system/general/"
$APIFTP = "/api/v2.0/ftp/"

$CertificateKey = Get-Content $CertificateKey -Raw
$CertificateChain = Get-Content $CertificateChain -Raw

if ($null -eq $CertificateKey) {
  Write-Error "Certificate Key File not found!" -ErrorAction Stop
}
elseif ($null -eq $CertificateChain) {
  Write-Error "Certificate Chain File not found!" -ErrorAction Stop
}

### Declare Functions
# Invoke WebSession
function Invoke-WebSession {
  Invoke-WebRequest ($ServerSocket + "/api/v2.0/") `
    -SkipCertificateCheck `
    -SessionVariable "WebSession" `
    -Authentication Bearer `
    -Token $APIKey
  Set-Variable WebSession $WebSession -Scope Global
}

# Get Certificate List
function Get-CertificateList {
  $Response = Invoke-WebRequest ($ServerSocket + $APICertificate) `
    -SkipCertificateCheck `
    -WebSession $WebSession `
    -ContentType "application/json" `
    -Body @{limit = 0 }
  $Response = ($Response.Content | ConvertFrom-Json)
  $Response
}

# Remove Old Certificate
function Remove-Certificate($CertificateName) {
  foreach ($Certificate in Get-CertificateList) {
    if ($Certificate.name -eq $CertificateName) {
      Invoke-WebRequest ($ServerSocket + $APICertificate + "id/" + $Certificate.id) `
        -SkipCertificateCheck `
        -WebSession $WebSession `
        -Method Delete
    }
  }
  Start-Sleep 5
}

# Import New Certificate
function Import-Certificate {
  Invoke-WebRequest ($ServerSocket + $APICertificate) `
    -SkipCertificateCheck `
    -WebSession $WebSession `
    -ContentType "application/json" `
    -Method Post `
    -Body (@{
      create_type = "CERTIFICATE_CREATE_IMPORTED"
      name        = "$CertificateName"
      certificate = "$CertificateChain"
      privatekey  = "$CertificateKey"
    } | ConvertTo-Json)
  Start-Sleep 5
}

# Get New Certificate ID
function Get-CertificateID($CertificateName) {
  foreach ($Certificate in Get-CertificateList) {
    if ($Certificate.name -eq $CertificateName) {
      $Certificate.id
    }
  }
}

# Set New Certificate
function Set-Certificate($CertificateID) {
  Invoke-WebRequest ($ServerSocket + $APISystemGeneral) `
    -SkipCertificateCheck `
    -WebSession $WebSession `
    -ContentType "application/json" `
    -Method Put `
    -Body (@{
      ui_certificate = $CertificateID
    } | ConvertTo-Json)
}

# Set New Certificate for FTP plugin
function Set-FTPCertificate($CertificateID) {
  Invoke-WebRequest ($ServerSocket + $APIFTP) `
    -SkipCertificateCheck `
    -WebSession $WebSession `
    -ContentType "application/json" `
    -Method Put `
    -Body (@{
      ssltls_certificate = $CertificateID
    } | ConvertTo-Json)
}

function Restart-UI { 
  Invoke-WebRequest ($ServerSocket + $APISystemGeneral + "ui_restart") `
    -SkipCertificateCheck `
    -WebSession $WebSession
}

### Update Certificate
Write-Host "Invoke-Websession"
Invoke-WebSession | Out-Null
Write-Host "Set-Certificate"
Set-Certificate(Get-CertificateID($CertificateFallbackName)) | Out-Null
if ($FTPused -eq "true") { Set-FTPCertificate(Get-CertificateID($CertificateFallbackName)) | Out-Null } 
Write-Host "Remove-Certificate"
Remove-Certificate($CertificateName) | Out-Null
Write-Host "Import-Certificate"
Import-Certificate | Out-Null
Write-Host "Set-Certificate"
Set-Certificate(Get-CertificateID($CertificateName)) | Out-Null
Write-Host "Set-FTPCertificate"
if ($FTPused -eq "true") { Set-FTPCertificate(Get-CertificateID($CertificateName)) | Out-Null } 
Write-Host "Restart-UI"
Restart-UI | Out-Null