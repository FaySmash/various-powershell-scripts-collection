<#PSScriptInfo
.VERSION 1.0.0
.AUTHOR FaySmash (https://gitlab.com/FaySmash)
.PROJECTURI https://gitlab.com/FaySmash
#>
<#
.SYNOPSIS
Download a File

.DESCRIPTION
Downloads a File from an URL

.PARAMETER URL
URL to the File

.PARAMETER DownloadDest
Download Destination to for the File

.PARAMETER Type
The Type of the Website:
NeedsJS - for Downloads which needs JavaScript
#>

Function Get-WebFile ($URL, $DownloadDest, $Type) {
    if ($Type -eq "NeedsJS") {
        # Import the Selenium WebDriver
        Import-Module "$($PSScriptRoot)\WebDriver.dll"

        $MSEdgeDriver = New-Object OpenQA.Selenium.Edge.EdgeDriver
        $MSEdgeDriver.Navigate().GoToURL($URL)

        Start-Sleep 10
        
        $Downloads = (New-Object -ComObject Shell.Application).NameSpace('shell:Downloads').Self.Path
        $Download = Get-ChildItem $Downloads | Sort-Object LastAccessTime -Descending | Select-Object -First 1
        Move-Item $Download.FullName $DownloadDest
    }
    else {
        (New-Object Net.WebClient).DownloadFile($URL, $DownloadDest)
    }
}