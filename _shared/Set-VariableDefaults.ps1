<#PSScriptInfo
.VERSION 1.0.0
.AUTHOR FaySmash (https://gitlab.com/FaySmash)
.PROJECTURI https://gitlab.com/FaySmash
#>
<#
.SYNOPSIS
Set the Default Variable Value

.DESCRIPTION
Set the Default Variable Value and store it in a Config File

.PARAMETER VarName
Variable Name

.PARAMETER VarVal
Variable default Value

.PARAMETER SetConfig
Variable default Value
#>


Function Set-VariableDefaults($VarName, $VarVal, $SetConfig) {
    $CurVarVal = Get-Variable -Name $VarName -ErrorAction Ignore
    if ($null -eq $CurVarVal) {
        Set-Variable -Name $VarName -Value $VarVal -Scope Global
        if ($SetConfig -eq $true) {
            Set-Config -VarToStore $VarName -ConfigPath $ConfigPath -Delimiter "="
        }
    }
}