<#PSScriptInfo
.VERSION 1.0.0
.AUTHOR FaySmash (https://gitlab.com/FaySmash)
.PROJECTURI https://gitlab.com/FaySmash
#>
<#
.SYNOPSIS
Store Config

.DESCRIPTION
Store Variables in a Config file

.PARAMETER ConfigPath
This Parameter defines the Path to the Config File

.PARAMETER Delimiter
This Parameter defines the Delimiter to use, to seperate the Variable Name from its assigned Value
#>


Function Set-Config($VarToStore, $ConfigPath, $Delimiter) {
    $ConfigContent = (Get-Content $ConfigPath -ErrorAction Ignore)
    Remove-Item $ConfigPath -ErrorAction Ignore
   
    $ContainsConfig = $ConfigContent | ForEach-Object {
        $_ -match $VarToStore
    }

    if ($ContainsConfig -contains $true) {
        foreach ($Config in $ConfigContent) {
            $Var = Get-Variable $VarToStore
                
            $OldValue = $Var.Name + "$Delimiter.*"
            $NewValue = $Var.Name + $Delimiter + $Var.Value
            $Config -replace ($OldValue, $NewValue) | Out-File $ConfigPath -Append -Encoding utf8
        }
    }
    else {
        $Var = Get-Variable $VarToStore
      
        $ConfigContent | Out-File $ConfigPath -Append -Encoding utf8
        $Var.Name + $Delimiter + $Var.Value | Out-File $ConfigPath -Append -Encoding utf8 -NoNewline
    }
}