<#PSScriptInfo
.VERSION 1.0.0
.AUTHOR FaySmash (https://gitlab.com/FaySmash)
.PROJECTURI https://gitlab.com/FaySmash
#>
<#
.SYNOPSIS
Crawls the HTML of a Website

.DESCRIPTION
Crawls the HTML of a Website for a specified RegEx

.PARAMETER WebsiteURL
The URL of the Website to Crawl

.PARAMETER RegExToMatch
The RegEx to find on the Website

.PARAMETER Type
The Type of the Website:
LargeWebsite - for lange and complex Websites
SmallWebsite - for small and complex Websites
SimpleWebsite - for small and simple Websites
#>


Function Get-WebsiteString($WebsiteURL, $RegExToMatch, $Type) {
    if ($Type -ne "SimpleWebsite") {
        # Import the Selenium WebDriver
        Import-Module "$($PSScriptRoot)\WebDriver.dll"

        $MSEdgeDriver = New-Object OpenQA.Selenium.Edge.EdgeDriver
        $MSEdgeDriver.Navigate().GoToURL($WebsiteURL)

        if ($Type -eq "LargeWebsite") {
            $MSEdgeDriver.PageSource | Out-File .\WebsiteContent.html
        }
        elseif ($Type -eq "SmallWebsite") {
            Set-Variable -Name "WebsiteContent" -Value $MSEdgeDriver.PageSource -Scope Global
        }

        $MSEdgeDriver.Close()
    }

    switch ($Type) {
        "LargeWebsite" {
            Get-Content .\WebsiteContent.html | ForEach-Object {
                if ($_ -match $RegExToMatch) {
                    return $matches[0]
                }
            }
            Remove-Item .\WebsiteContent.html -Force
        }
        "SmallWebsite" {
            $WebsiteContent | ForEach-Object {
                if ($_ -match $RegExToMatch) {
                    return $matches[0]
                }
            }
        }
        "SimpleWebsite" {
            (Invoke-WebRequest $WebsiteURL) | ForEach-Object {
                if ($_ -match $RegExToMatch) {
                    return $matches[0]
                }
            }
        }
    }
}