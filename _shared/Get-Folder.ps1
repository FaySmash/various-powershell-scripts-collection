<#PSScriptInfo
.VERSION 1.0.0
.AUTHOR FaySmash (https://gitlab.com/FaySmash)
.PROJECTURI https://gitlab.com/FaySmash
#>
<#
.SYNOPSIS
Opens a Folder-Browser Dialog

.DESCRIPTION
Opens a Folder-Browser Dialog and returns the selected Path

.PARAMETER initialDirectory
This Parameter defines the Path, the Folder-Browser Dialog starts in
#>

Function Get-Folder($initialDirectory) {
    Add-Type -AssemblyName System.Windows.Forms

    $FolderBrowserDialog = New-Object System.Windows.Forms.FolderBrowserDialog -Property @{
        SelectedPath        = $initialDirectory
        ShowNewFolderButton = $false
    }

    $script:DialogResult = $FolderBrowserDialog.ShowDialog()
    $FolderBrowserDialog.SelectedPath
}