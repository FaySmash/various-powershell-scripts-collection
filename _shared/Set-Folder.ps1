<#PSScriptInfo
.VERSION 1.0.0
.AUTHOR FaySmash (https://gitlab.com/FaySmash)
.PROJECTURI https://gitlab.com/FaySmash
#>
<#
.SYNOPSIS
Calls Get-Folder to open the Folder-Browser Dialog

.DESCRIPTION
Calls Get-Folder to open the Folder-Browser Dialog, sets the result Path as Variable

.PARAMETER FolderPathVariable
Variable, the picked Path should be assigned too
#>

Function Set-Folder($FolderPathVariable) {
    Set-Variable -Name $FolderPathVariable -Value (Get-Folder -initialDirectory (Get-Variable $FolderPathVariable -ValueOnly))
    if ($DialogResult -eq "OK") {
        Set-Config -VarToStore $FolderPathVariable -ConfigPath $ConfigPath -Delimiter "="
        return (Get-Variable $FolderPathVariable -ValueOnly)
    }
    else {
        break
    }
}