<#PSScriptInfo
.VERSION 1.0.0
.AUTHOR FaySmash (https://gitlab.com/FaySmash)
.PROJECTURI https://gitlab.com/FaySmash
#>
<#
.SYNOPSIS
Creats missing Folders

.DESCRIPTION
# Function to create Folders if they don't exist

.PARAMETER Dir
Directory to test for
#>

Function Test-DestDir ($Dir) {
    if (!(Test-Path -Path ($Dir))) {
        New-Item ($Dir) -ItemType Directory | Out-Null
    }
}