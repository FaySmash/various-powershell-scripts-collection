<#PSScriptInfo
.VERSION 1.0.0
.AUTHOR FaySmash (https://gitlab.com/FaySmash)
.PROJECTURI https://gitlab.com/FaySmash
#>
<#
.SYNOPSIS
Load stored Config

.DESCRIPTION
Load stored Variables from a Config file, created with Set-Config.ps1

.PARAMETER ConfigPath
This Parameter defines the Path to the Config File

.PARAMETER Delimiter
This Parameter defines the Delimiter to use, to seperate the Variable Name from its assigned Value
#>

Function Get-Config($ConfigPath, $Delimiter) {
    $Config = Get-Content $ConfigPath -ErrorAction Ignore
    $Config | ForEach-Object {
        $preset = $_.Split($Delimiter)
        Set-Variable -Name $preset[0] -Value $preset[1] -Scope Global
    }
}