﻿Set-Location $PSScriptRoot

### Initializes Variables
# Path of the File containing stored Variables
$ConfigPath = ".\Ressources\config.txt"

. ..\_shared\Get-Config.ps1

. Get-Config -ConfigPath $ConfigPath -Delimiter "="

### Declare Functions
# Function to Store Variables in a File
. ..\_shared\Set-Config.ps1

# Function to Initialize Variables, if they're not present in the Config-File
. ..\_shared\Set-VariableDefaults.ps1

# Function to get the newest Online Version Number and Download Link by HTML sniffing
. ..\_shared\Get-WebsiteString.ps1

# Function to Open the Folder Browser
. ..\_shared\Get-Folder.ps1

# Function to Download Files
. ..\_shared\Get-WebFile.ps1

# Function to create Folders if they don't exist
. ..\_shared\Test-DestDir.ps1

# Function to create Folders if they don't exist
. ..\_shared\Test-DestDir.ps1

# Function to Open the Game-Folder Browser
Function Get-GameFolder {
    $GameFolder = Get-Folder -initialDirectory $GameFolder
    if ($DialogResult -eq "OK") {
        Set-Config -VarToStore "GameFolder" -ConfigPath $ConfigPath -Delimiter "="
        return $GameFolder
    }
    else {
        break
    }
}

# Function to Copy the GoldbergEMU DLLs with the right name
Function Copy-Binaries ($GoldbergEMUDLL) { 
    Switch ($GoldbergEMUDLL.Name) {
        "steam_api.dll" { Copy-Item -Path ($CurrentVersionPath + "\Binaries\steam_api.dll") -Destination ($GoldbergEMUDLL.DirectoryName + "\steam_api.dll") }
        "steam_api64.dll" { Copy-Item -Path ($CurrentVersionPath + "\Binaries\steam_api64.dll") -Destination ($GoldbergEMUDLL.DirectoryName + "\steam_api64.dll") }
    }  
}

Function Copy-Misc ($GoldbergEMUUpdateMarker){
    Copy-Item -Path (".\Ressources\" + $ToCopyDir + "\*") -Destination $GoldbergEMUUpdateMarker.DirectoryName -Recurse -Force
}


### Declare Variables
# Website from where to get the newest GoldbergEMU Version
. Set-VariableDefaults -VarName "GoldbergEMUGitLabWebsite" -VarVal "https://mr_goldberg.gitlab.io/goldberg_emulator" -SetConfig $true

# Determing the Prefix of the GoldbergEMU Download URL
. Set-VariableDefaults -VarName "GoldbergEMUDownloadURL" -VarVal "https://gitlab.com/Mr_Goldberg/goldberg_emulator/" -SetConfig $true

# Determing the Current Version and Download URL
$GoldbergEMUDownloadURLSuffix = Get-WebsiteString -WebsiteURL $GoldbergEMUGitLabWebsite -RegExToMatch "-/jobs/.*/artifacts/download" -Type "SimpleWebsite"
$Version = $GoldbergEMUDownloadURLSuffix -Replace ("-/jobs/", "") -Replace ("/artifacts/download", "")
$GoldbergEMUDownloadURL = $GoldbergEMUDownloadURL + $GoldbergEMUDownloadURLSuffix

# Determing the Path with the Current Version Number
$CurrentVersionPath = "Versions\" + $Version

# Determing the Destination/Extraction Paths for GoldbergEMU
$GoldbergEMUDownloadDest = $PSScriptRoot + "\" + $CurrentVersionPath + "\GoldbergEMU.zip"

# Determing the Parent-Destination for the Game Folders
$GoldbergEMUDest = Get-GameFolder


### Get Updates
# Creates Destination Directories
Test-DestDir -Dir ($CurrentVersionPath + "\Binaries")

# Checks if a new Version is Avaliable. If not, stop proceeding with the Updating
if (!(Test-Path -Path ($CurrentVersionPath + "\Binaries\*"))) {
    Write-Host ("New Version " + $Version + " found!")
    Write-Host "-------------------------------------------------------"

    # Downloads GoldbergEMU
    Write-Host "Downloading GoldbergEMU..."
    Get-WebFile -URL $GoldbergEMUDownloadURL -DownloadDest $GoldbergEMUDownloadDest
    
    # Extracts GoldbergEMU
    Write-Host "Extracting GoldbergEMU"
    Expand-Archive -Path $GoldbergEMUDownloadDest -DestinationPath ($CurrentVersionPath + "\Binaries")
    Remove-Item ($CurrentVersionPath + "\Binaries\*") -Recurse -Exclude ("steam_api.dll", "steam_api64.dll")
    Remove-Item $GoldbergEMUDownloadDest
}
else {
    Write-Host "No New Version found, installing current Version"
    Write-Host "-------------------------------------------------------"
}

### Install Updates Code and Adjust Config File Code
# Triggers the Copying of the Binary Files into every Folder with a #GoldbergEMUUpdate.Marker File
$GoldbergEMUUpdateMarkers = Get-ChildItem -Path $GoldbergEMUDest -Recurse -Filter "*.Marker" -Include "#GoldbergEMUUpdate.Marker"

if ($null -eq $GoldbergEMUUpdateMarkers) {
    Write-Host "No Update Markers found, make sure to create a File named #GoldbergEMUUpdate.Marker in each Directory, you want to Update GoldbergEMU"
    break
}

Write-Host ("Update Marker found in: ")
$GoldbergEMUUpdateMarkers | ForEach-Object {
    Write-Host $_.DirectoryName
}

Write-Host "-------------------------------------------------------"
pause

foreach ($GoldbergEMUUpdateMarker in $GoldbergEMUUpdateMarkers) {
    Write-Host ("Updatiung GoldbergEMU in " + $GoldbergEMUUpdateMarker.DirectoryName)

    $GoldbergEMUDLLs = Get-ChildItem -Path $GoldbergEMUUpdateMarker.DirectoryName -Recurse -Filter "*.dll" -Include "steam_api.dll", "steam_api64.dll"
    foreach ($GoldbergEMUDLL in $GoldbergEMUDLLs) {
        Copy-Binaries ($GoldbergEMUDLL)
    }

    Write-Host ("Copy Misc Files")
    Write-Host ">>>>>>>>>>"
    Copy-Misc ($GoldbergEMUUpdateMarker)
}
pause