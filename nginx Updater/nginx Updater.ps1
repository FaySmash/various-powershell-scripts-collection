﻿Set-Location $PSScriptRoot

# Run as Admin
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Start-Process powershell.exe "-File", ('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
    exit
}

### Initializes Variables
# Path of the File containing stored Variables
$ConfigPath = ".\Ressources\config.txt"

. ..\_shared\Get-Config.ps1

. Get-Config -ConfigPath $ConfigPath -Delimiter "="

### Declare Functions
# Function to Store Variables in a File
. ..\_shared\Set-Config.ps1

# Function to Initialize Variables, if they're not present in the Config-File
. ..\_shared\Set-VariableDefaults.ps1

# Function to get the newest Online Version Number and Download Link by HTML sniffing
. ..\_shared\Get-WebsiteString.ps1

# Function to Open the Folder Browser
. ..\_shared\Get-Folder.ps1

# Function to optain the Folders with Get-Folder
. ..\_shared\Set-Folder.ps1

# Function to Download Files
. ..\_shared\Get-WebFile.ps1

# Function to create Folders if they don't exist
. ..\_shared\Test-DestDir.ps1


### Declare Variables
# Website from where to get the newest nginx Version
. Set-VariableDefaults -VarName "nginxWebsite" -VarVal "http://nginx.org/en/download.html" -SetConfig $true

# Determing the Prefix of the nginx Download URL
. Set-VariableDefaults -VarName "nginxDownloadURL" -VarVal "http://nginx.org/download/" -SetConfig $true

# Determing the Current Version and Download URL
$nginxDownloadURLSuffix = Get-WebsiteString -WebsiteURL $nginxWebsite -RegExToMatch "/download/nginx-.*?.zip" -Type "SimpleWebsite"
$nginxDownloadURLSuffix = $nginxDownloadURLSuffix -replace ("\/download\/nginx-.*.tar.gz`">nginx-.*<\/a>\?\? <a href=`"\/download\/nginx-.*.tar.gz.asc`">pgp<\/a><\/td><td><a href=`"\/download\/", "")
$Version = $nginxDownloadURLSuffix -Replace ("nginx-", "") -Replace ("\.zip", "")
$nginxDownloadURL = $nginxDownloadURL + $nginxDownloadURLSuffix

# Determing the Path with the Current Version Number
$CurrentVersionPath = "Versions\" + $Version

# Determing the Destination/Extraction Paths for nginx
$nginxDownloadDest = $PSScriptRoot + "\" + $CurrentVersionPath + "\nginx.zip"

# Determing the Parent-Destination for the Game Folders
$nginxDest = Set-Folder -FolderPathVariable "nginxFolder"


### Get Updates
# Creates Destination Directories
Test-DestDir -Dir $CurrentVersionPath

# Checks if a new Version is Avaliable. If not, stop proceeding with the Updating
if (!(Test-Path -Path "$CurrentVersionPath\*")) {
    Write-Host ("New Version " + $Version + " found!")
    Write-Host "-------------------------------------------------------"

    # Downloads nginx
    Write-Host "Downloading nginx..."
    Get-WebFile -URL $nginxDownloadURL -DownloadDest $nginxDownloadDest
    
    # Extracts nginx
    Write-Host "Extracting nginx"
    Expand-Archive -Path $nginxDownloadDest -DestinationPath $CurrentVersionPath
    Move-Item ($CurrentVersionPath + "\nginx-$Version\*") $CurrentVersionPath
    Remove-Item $nginxDownloadDest, ($CurrentVersionPath + "\nginx-$Version"), ($CurrentVersionPath + "\conf") -Recurse
}
else {
    Write-Host "No New Version found"
    Pause
    break
}

### Install Updates
# Backups the Current Binaries
& xcopy "$nginxDest" "$nginxDest old\" /E /Y

# Triggers the Copying of the Binary Files into the nginxFolder
& "C:\Tools\System Tools\NSSM\nssm.exe" stop nginx
Write-Host "Copy Files..."
Copy-Item "$CurrentVersionPath\*" $nginxDest -Recurse -Force
& "C:\Tools\System Tools\NSSM\nssm.exe" start nginx
Write-Host "Done"
pause