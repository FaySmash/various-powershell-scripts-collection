# Various PowerShell Scripts Collection

A Repository with a Collection of my various personal PowerShell Scripts, maybe they'll be useful for somebody else too.

# Usage

Just clone this Repo/download it and extract it somewhere.
Some Scripts have configuration (config.txt) Files besides them or in a Subfolder of the Script directory, Rename them from `Example#config.txt` to `config.txt` and adjust needed Parameters, **before the first run**. If you don't adjust them before the first run, the `config.txt` will be filled with Default Variable Values, defined inside the PowerShell Script.

# Quick Summary

* the _shared Folder holds some simple but useful Scripts, you can use in your Scripts by Importing them with a "."
    * for Get-WebFile.ps1 and Get-WebsiteString.ps1 the MSEdge WebDriver and Selenium 4 is required
        * get the msedgedriver **[here](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/)** and put it in the _shared folder
        * get the WebDriver **[here](https://www.selenium.dev/downloads/)** and put it in the _shared folder
* ReShade Updater asks you for a Folder when executed, select one Game Folder containg ReShade or a Folder/Partition holding all your Games. By default the Addon enabled Version is used, disable it in the config if you play online games, The Script then proceeds to scan for `#ReShadeUpdater.Marker` Files, which have to be placed at the same Folder where ReShades Binaries resident in. You need to Download UniExtract from **[here](https://www.legroom.net/scripts/download.php?file=uniextract161_noinst)** and put in inside a Folder named `UniExtract` inside the `Ressources` folder = `ReShade Updater\Ressources\UniExtract\UniExtract.exe`. The `ReShadeIniPresets.txt` is a possibility, to apply default Values to the` ReShade.ini` File. This Script Scans through the `ReShade.ini` and replaces every Value defined in the `ReShadeIniPresets.txt` with the chosen Value. If the `EffectSearchPaths=` and `TextureSearchPaths=` Variables are changed to a different location, the Shaders or Textures will only be downloaded to the defined location, else they're put in every Games Folder. If you don't want to use this Feature, delete the whole File Content.
* GoldbergEMU Updater asks you for a Folder when executed, select one Game Folder containg GoldbergEMU or a Folder/Partition holding all your Games. The Script then proceeds to scan for `#GoldbergEMUUpdater.Marker` Files, which have to be placed at the same Folder where GoldbergEMU Binaries resident in. You can also put Files you want to be copied to every Directory holding a `#GoldbergEMUUpdater.Marker` File inside the `/Ressources/to_copy` Directory, e.g. a `local_save.txt`.
* nginx Updater asks you for a Folder when executed, select the Folder wher `nginx.exe` is located. A Backup of the Folder which holds `nginx.exe` will be created in the same Directory with a "old" Suffix. The Script is configured to Terminate my nginx NSSM Service and restart it after the Update, you may have to adjust those lines.
* WinACME Updater asks you for a Folder when executed, select the Folder wher `wacs.exe` is located. Backup of the Folder which holds `wacs.exe` will be created in the same Directory with a "old" Suffix.
* TrueNAS Certificates allows you to upload your own certificates to your TrueNAS instance via its REST API
