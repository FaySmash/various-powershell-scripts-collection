﻿Set-Location $PSScriptRoot

### Initializes Variables
# Path of the File containing stored Variables
$ConfigPath = ".\Ressources\config.txt"

. ..\_shared\Get-Config.ps1

. Get-Config -ConfigPath $ConfigPath -Delimiter "="

$ReShadeIniPresetsArray = @{ }
$ReShadeIniPresets = Get-Content ".\Ressources\ReShadeIniPresets.txt"
$ReShadeIniPresets | ForEach-Object {
    $preset = $_.Split("=")
    $ReShadeIniPresetsArray.Add($preset[0], $preset[1])
}

### Declare Functions
# Function to Store Variables in a File
. ..\_shared\Set-Config.ps1

# Function to Initialize Variables, if they're not present in the Config-File
. ..\_shared\Set-VariableDefaults.ps1

# Function to get the newest Online Version Number and Download Link by HTML sniffing
. ..\_shared\Get-WebsiteString.ps1

# Function to Open the Folder Browser
. ..\_shared\Get-Folder.ps1

# Function to optain the Folders with Get-Folder
. ..\_shared\Set-Folder.ps1

# Function to Download Files
. ..\_shared\Get-WebFile.ps1

# Function to create Folders if they don't exist
. ..\_shared\Test-DestDir.ps1

# Function to Copy the ReShade DLLs with the right name
Function Copy-Binaries ($ReShadeDLL, $Bits) {
        Copy-Item -Path ($CurrentVersionPath + "\Binaries\ReShade" + $Bits + ".dll") -Destination $ReShadeDLL.FullName
}

# Function to Copy the ReShade Shaders
Function Copy-ShadersAndTextures ($ReShadeUpdateMarker) {
    if ($ReShadeIniPresetsArray["EffectSearchPaths"] -eq ".\reshade-shaders\Shaders") {
        Remove-Item ($ReShadeUpdateMarker.DirectoryName + "\reshade-shaders\Shaders") -Recurse -ErrorAction Ignore
        Copy-Item -Path ".\reshade-shaders\Shaders" -Destination ($ReShadeUpdateMarker.DirectoryName + "\reshade-shaders\Shaders") -Recurse
        Copy-Item -Path ".\custom-reshade-shaders\Shaders\*" -Destination ($ReShadeUpdateMarker.DirectoryName + "\reshade-shaders\Shaders") -Recurse
    }
    else {
        Remove-Item $ReShadeIniPresetsArray["EffectSearchPaths"] -Recurse -ErrorAction Ignore
        Copy-Item -Path ".\reshade-shaders\Shaders" -Destination $ReShadeIniPresetsArray["EffectSearchPaths"] -Recurse
        Copy-Item -Path ".\custom-reshade-shaders\Shaders\*" -Destination $ReShadeIniPresetsArray["EffectSearchPaths"] -Recurse
    }
    
    if ($ReShadeIniPresetsArray["TextureSearchPaths"] -eq ".\reshade-shaders\Textures") {
        Remove-Item ($ReShadeUpdateMarker.DirectoryName + "\reshade-shaders\Textures") -Recurse -ErrorAction Ignore
        Copy-Item -Path ".\reshade-shaders\Textures" -Destination ($ReShadeUpdateMarker.DirectoryName + "\reshade-shaders\Textures") -Recurse
        Copy-Item -Path ".\custom-reshade-shaders\Textures\*" -Destination ($ReShadeUpdateMarker.DirectoryName + "\reshade-shaders\Textures") -Recurse
    }
    else {
        Remove-Item $ReShadeIniPresetsArray["TextureSearchPaths"] -Recurse -ErrorAction Ignore
        Copy-Item -Path ".\reshade-shaders\Textures" -Destination $ReShadeIniPresetsArray["TextureSearchPaths"] -Recurse
        Copy-Item -Path ".\custom-reshade-shaders\Textures\*" -Destination $ReShadeIniPresetsArray["TextureSearchPaths"] -Recurse
    }
}

# Function to Apply Presets to the INIs
Function Edit-Ini ($ReShadeUpdateMarker) {
    $IniName = $ReShadeUpdateMarker.DirectoryName + "\ReShade.ini"

    $IniNameOld = $ReShadeUpdateMarker.DirectoryName + "\ReShade.ini"
    
    $IniContent = (Get-Content $IniNameOld)
    Remove-Item $IniNameOld

    foreach ($CurrentLine in $IniContent) {
        $CurrentPreset = $CurrentLine.Split("=")
        $OldValue = $CurrentPreset[0] + "=.*"
        $NewValue = $CurrentPreset[0] + "=" + $ReShadeIniPresetsArray[$CurrentPreset[0]]
        if ($ReShadeIniPresetsArray.Contains($CurrentPreset[0])) {
            $CurrentLine -replace $OldValue, $NewValue | Out-File $IniName -Append -Encoding utf8
        }
        else {
            $CurrentLine | Out-File $IniName -Append -Encoding utf8
        }
    }
}


### Declare Variables
# Determing, if the Addon Version of ReShade should be used
. Set-VariableDefaults -VarName "AddonVersion" -VarVal $true -SetConfig $true

# Website from where to get the newest ReShade Version
. Set-VariableDefaults -VarName "ReShadeWebsite" -VarVal "https://reshade.me/" -SetConfig $true

# Website from where to get the newest ReShade-Shader Versions
. Set-VariableDefaults -VarName "ReShadeShaderDownloadURL" -VarVal "https://github.com/crosire/reshade-shaders/archive/master.zip" -SetConfig $true

# Determing the location of 3rd Party Tools
. Set-VariableDefaults -VarName "UniExtract" -VarVal ".\Ressources\UniExtract\UniExtract.exe" -SetConfig $true

# Determing the Current Version and Download URL
$Version = Get-WebsiteString -WebsiteURL $ReShadeWebsite -RegExToMatch "Version [0-9]\.[0-9]\.[0-9]" -Type "SimpleWebsite"
$Version = $Version -replace ("Version ", "")
$ReShadeDownloadURL = $ReShadeWebsite + "downloads/ReShade_Setup_" + $Version
if ($AddonVersion = $true) {
    $ReShadeDownloadURL = $ReShadeDownloadURL + "_Addon.exe"
}
else {
    $ReShadeDownloadURL = $ReShadeDownloadURL + ".exe"
}

# Determing the Path with the Current Version Number
$CurrentVersionPath = "Versions\" + $Version

# Determing the Destination/Extraction Paths for ReShade
$ReShadeDownloadDest = $PSScriptRoot + "\" + $CurrentVersionPath + "\ReShade.exe"
$ReShadeDownloadDestPath = "`"$ReShadeDownloadDest`""

# Determing the Destination/Extraction Paths for the downloaded ReShade-Shaders
$ReShadeShaderDownloadDest = $PSScriptRoot + "\reshade-shaders\master.zip"

# Determing the Parent-Destination for the Game Folders
$ReShadeDest = Set-Folder -FolderPathVariable "GameFolder"


### Get Updates
# Creates Destination Directories
Test-DestDir -Dir ($CurrentVersionPath + "\Binaries")
Test-DestDir -Dir "reshade-shaders"

# Checks if a new Version is Avaliable. If not, stop proceeding with the Updating
if (!(Test-Path -Path ($CurrentVersionPath + "\Binaries\*"))) {
    Write-Host ("New Version " + $Version + " found!")
    Write-Host "-------------------------------------------------------"

    # Downloads ReShade and its Shaders
    Write-Host "Downloading Shaders..."
    Get-WebFile -URL $ReShadeShaderDownloadURL -DownloadDest $ReShadeShaderDownloadDest
    Write-Host "Downloading ReShade..."
    Get-WebFile -URL $ReShadeDownloadURL -DownloadDest $ReShadeDownloadDest -Type $DownloadType
    
    # Extracts ReShade
    while (!(Test-Path $ReShadeDownloadDest)) { Start-Sleep 2 }

    Write-Host "Extracting ReShade"
    Start-Process $UniExtract -ArgumentList $ReShadeDownloadDestPath, "$CurrentVersionPath\Binaries" -Wait
    Remove-Item ($CurrentVersionPath + "\Binaries\*") -Recurse -Exclude ("ReShade32.dll", "ReShade64.dll")
    Remove-Item $ReShadeDownloadDest

    # Extracts the ReShade-Shaders Archive and deletes it afterward
    Write-Host "Extracting Shaders"
    Remove-Item ".\reshade-shaders\*" -Recurse -Exclude "master.zip"
    Expand-Archive -Path $ReShadeShaderDownloadDest -DestinationPath ".\reshade-shaders"
    Move-Item -Path ".\reshade-shaders\reshade-shaders-slim\Shaders", ".\reshade-shaders\reshade-shaders-slim\Textures" -Destination ".\reshade-shaders"
    Remove-Item -Path ".\reshade-shaders\reshade-shaders-slim", $ReShadeShaderDownloadDest -Recurse
}
else {
    Write-Host "No New Version found, installing current Version"
    Write-Host "-------------------------------------------------------"
}

### Install Updates Code and Adjust Config File Code
# Triggers the Copying of the Binary and Shader Files into every Folder with a #ReShadeUpdate.Marker File
Write-Host ("Searching Update Markers...")
$ReShadeUpdateMarkers = Get-ChildItem -Path $ReShadeDest -Recurse -Filter "*.Marker" -Include "#ReShadeUpdate.Marker"

if ($null -eq $ReShadeUpdateMarkers) {
    Write-Host "No Update Markers found, make sure to create a File named #ReShadeUpdate.Marker in each Directory, you want to Update ReShade"
    break
}

Write-Host ("Update Marker found in: ")
$ReShadeUpdateMarkers | ForEach-Object {
    Write-Host $_.DirectoryName
}

Write-Host "-------------------------------------------------------"
pause

foreach ($ReShadeUpdateMarker in $ReShadeUpdateMarkers) {
    Write-Host ("Updating ReShade in " + $ReShadeUpdateMarker.DirectoryName)
    
    $ReShadeDLLs = Get-ChildItem -Path ($ReShadeUpdateMarker.DirectoryName + "\*") -Include "d3d9.dll", "d3d11.dll", "dxgi.dll", "opengl32.dll", "dinput8.dll", "ReShade32.dll", "ReShade64.dll"
    
    foreach ($ReShadeDLL in $ReShadeDLLs) {
        $DLLContent = Get-Content $ReShadeDLL -Raw

        if ($DLLContent.Contains("crosire's ReShade post-processing injector")) {
            if ($DLLContent.Substring(0, 300) -cmatch "PE..L") {
                Copy-Binaries $ReShadeDLL "32"
            }
            else {
                Copy-Binaries $ReShadeDLL "64"
            }
        }
    }

    Copy-ShadersAndTextures ($ReShadeUpdateMarker)
    Write-Host ("Adjusting Config File in " + $ReShadeUpdateMarker.DirectoryName)
    Write-Host ">>>>>>>>>>"
    Edit-Ini ($ReShadeUpdateMarker)
}
pause