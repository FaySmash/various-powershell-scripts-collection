﻿Set-Location $PSScriptRoot

# Run as Admin
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Start-Process powershell.exe "-File", ('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
    exit
}

### Initializes Variables
# Path of the File containing stored Variables
$ConfigPath = ".\Ressources\config.txt"

. ..\_shared\Get-Config.ps1

. Get-Config -ConfigPath $ConfigPath -Delimiter "="

### Declare Functions
# Function to Store Variables in a File
. ..\_shared\Set-Config.ps1

# Function to Initialize Variables, if they're not present in the Config-File
. ..\_shared\Set-VariableDefaults.ps1

# Function to get the newest Online Version Number and Download Link by HTML sniffing
. ..\_shared\Get-WebsiteString.ps1

# Function to Open the Folder Browser
. ..\_shared\Get-Folder.ps1

# Function to optain the Folders with Get-Folder
. ..\_shared\Set-Folder.ps1

# Function to Download Files
. ..\_shared\Get-WebFile.ps1

# Function to create Folders if they don't exist
. ..\_shared\Test-DestDir.ps1


### Declare Variables
# Website from where to get the newest WinACME Version
. Set-VariableDefaults -VarName "WinACMEGitHubWebsite" -VarVal "https://github.com/win-acme/win-acme/releases/latest" -SetConfig $true

# Determing the Prefix of the WinACME Download URL
. Set-VariableDefaults -VarName "WinACMEDownloadURL" -VarVal "https://github.com/win-acme/win-acme/releases/download/" -SetConfig $true

# Determing the Current Version and Download URL
$WinACMEDownloadURLSuffix = Get-WebsiteString -WebsiteURL $WinACMEGitHubWebsite -RegExToMatch "v.*\/win-acme\.v.*x64\.trimmed\.zip" -Type "SimpleWebsite"
$Version = $WinACMEDownloadURLSuffix -Replace ("v.*\/win-acme\.v", "") -Replace ("\.x64\.trimmed\.zip", "")
$WinACMEDownloadURL = $WinACMEDownloadURL + $WinACMEDownloadURLSuffix

# Determing the Path with the Current Version Number
$CurrentVersionPath = "Versions\" + $Version

# Determing the Destination/Extraction Paths for WinACME
$WinACMEDownloadDest = $PSScriptRoot + "\" + $CurrentVersionPath + "\WinACME.zip"

# Determing the Parent-Destination for the Game Folders
$WinACMEDest = Set-Folder -FolderPathVariable "WinACMEFolder"


### Get Updates
# Creates Destination Directories
Test-DestDir -Dir $CurrentVersionPath

# Checks if a new Version is Avaliable. If not, stop proceeding with the Updating
if (!(Test-Path -Path "$CurrentVersionPath\*")) {
    Write-Host ("New Version " + $Version + " found!")
    Write-Host "-------------------------------------------------------"

    # Downloads WinACME
    Write-Host "Downloading WinACME..."
    Get-WebFile -URL $WinACMEDownloadURL -DownloadDest $WinACMEDownloadDest
    
    # Extracts WinACME
    Write-Host "Extracting WinACME"
    Expand-Archive -Path $WinACMEDownloadDest -DestinationPath $CurrentVersionPath
    Remove-Item $WinACMEDownloadDest
}
else {
    Write-Host "No New Version found"
    Pause
    break
}

### Install Updates
# Backups the Current Binaries
& xcopy "$WinACMEDest" "$WinACMEDest old\" /E /Y

# Triggers the Copying of the Binary Files into the WinACMEFolder
    Write-Host "Copy Files..."
    Copy-Item "$CurrentVersionPath\*" $WinACMEDest -Recurse -Force
    Write-Host "Done"
pause